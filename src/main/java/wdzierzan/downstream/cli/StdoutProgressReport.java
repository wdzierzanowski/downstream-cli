/*
 * Copyright (C) 2011 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.cli;

import wdzierzan.downstream.core.ProgressReport;

/**
 *
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
class StdoutProgressReport implements ProgressReport {

    public void updateProgress(int total, int current) {
        System.out.format("%d/%d (%.0f%%)\n", current, total, ((double) current * 100) / total);
    }

    public void updateRate(int kBps) {
        System.out.format("%d kB/s\n", kBps);
    }

    public long getRateUpdatePeriod() {
        return 2000;
    }
}
