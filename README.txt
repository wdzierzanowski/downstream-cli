
downstream-cli
===============

https://bitbucket.org/wdzierzanowski/downstream-cli

downstream-cli is a simple command-line client of the downstream-core [1] Java
library.  While downstream-core was originally developed as the backend for the
Android application Downstream [2], downstream-cli can be used to test the
library without the overhead of using an Android device or emulator.

Please read the included LICENCE.txt.

[1] https://bitbucket.org/wdzierzanowski/downstream-core
[2] https://bitbucket.org/wdzierzanowski/downstream-android
